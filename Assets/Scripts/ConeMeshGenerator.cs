﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeMeshGenerator : MonoBehaviour
{
    [Min(3)]
    public int NumSides = 3;
    public int NumLayers = 5;
    public float Slope = 0.5f;
    public Texture2D HeightMap = null;
    public float HeightMapScale = 5.0f;
    public bool BilinearHeight = true;
    public bool QuantizeHeight = true;

    Mesh mesh = null;
    List<Vector3> meshVertices = new List<Vector3>();
    List<Vector3> meshNormals = new List<Vector3>();
    List<int> meshIndices = new List<int>();
    int currentIndex = 0;
    float cornerAngle = 0;

    public void Meshalize()
    {
        meshVertices.Clear();
        meshNormals.Clear();
        meshIndices.Clear();
        currentIndex = 0;
        cornerAngle = 2.0f * Mathf.PI / NumSides;

        CreateFirstLayer();

        for (int layer = 1; layer < NumLayers; ++layer)
        {
            for (int side = 0; side < NumSides; ++side)
            {
                CreateStrip(side, layer);
            }
        }

        CreateMesh();
    }

    private void CreateFirstLayer()
    {
        Vector3 center = Vector3.zero;
        for (int i = 0; i < NumSides; ++i)
        {
            float x1 = Mathf.Sin(i * cornerAngle);
            float z1 = Mathf.Cos(i * cornerAngle);
            float x2 = Mathf.Sin((i + 1) * cornerAngle);
            float z2 = Mathf.Cos((i + 1) * cornerAngle);
            Vector3 a = new Vector3(x1, 0.0f, z1);
            Vector3 b = new Vector3(x2, 0.0f, z2);

            CreateTriangle(center, a, b);
        }
    }

    private void CreateStrip(int side, int layer)
    {
        float x1 = Mathf.Sin((side) * cornerAngle);
        float z1 = Mathf.Cos((side) * cornerAngle);
        float x2 = Mathf.Sin((side + 1) * cornerAngle);
        float z2 = Mathf.Cos((side + 1) * cornerAngle);

        // Have the base line
        Vector3 base1 = (new Vector3(x1, 0.0f, z1) * layer);
        Vector3 base2 = (new Vector3(x2, 0.0f, z2) * layer);

        // Have the new line
        Vector3 new1 = (new Vector3(x1, 0.0f, z1) * (layer + 1));
        Vector3 new2 = (new Vector3(x2, 0.0f, z2) * (layer + 1));

        int numTriangles = 2 * layer + 1;
        int numBasePoints = layer;
        int numNewPoints = layer + 1;

        // Stripify between the lines
        for (int i = 0; i < numTriangles; ++i)
        {
            // TODO: reduce load by re-using previously calculated points
            if (i % 2 == 0)
            {
                Vector3 a = Vector3.LerpUnclamped(base1, base2, (float)(i / 2) / numBasePoints);
                Vector3 b = Vector3.LerpUnclamped(new1, new2, (float)(i / 2) / numNewPoints);
                Vector3 c = Vector3.LerpUnclamped(new1, new2, (float)(i / 2 + 1) / numNewPoints);

                CreateTriangle(a, b, c);
            }
            else
            {
                Vector3 a = Vector3.LerpUnclamped(base1, base2, (float)(i / 2) / numBasePoints);
                Vector3 b = Vector3.LerpUnclamped(new1, new2, (float)(i / 2 + 1) / numNewPoints);
                Vector3 c = Vector3.LerpUnclamped(base1, base2, (float)(i / 2 + 1) / numBasePoints);

                CreateTriangle(a, b, c);
            }
        }
    }

    private void CreateTriangle(Vector3 a, Vector3 b, Vector3 c)
    {
        a.y = GetHeight(a);
        b.y = GetHeight(b);
        c.y = GetHeight(c);

        Vector3 normal = Vector3.Cross(b - a, c - a).normalized;

        meshVertices.Add(a);
        meshVertices.Add(b);
        meshVertices.Add(c);

        meshNormals.Add(normal);
        meshNormals.Add(normal);
        meshNormals.Add(normal);

        meshIndices.Add(currentIndex++);
        meshIndices.Add(currentIndex++);
        meshIndices.Add(currentIndex++);
    }

    private float GetHeight(Vector3 point)
    {
        float distance = new Vector2(point.x, point.z).magnitude;
        float baseY = -distance * Slope;

        if (HeightMap == null)
        {
            return baseY;
        }

        float u = point.x / NumLayers * 0.5f + 0.5f;
        float v = point.z / NumLayers * 0.5f + 0.5f;

        float mapY;
        if (BilinearHeight)
        {
            mapY = HeightMap.GetPixelBilinear(u, v).r;
        }
        else
        {
            int x = Mathf.RoundToInt(u * HeightMap.width);
            int y = Mathf.RoundToInt(v * HeightMap.height);
            mapY = HeightMap.GetPixel(x, y).r;
        }

        float height = baseY + mapY * HeightMapScale;
        return QuantizeHeight ? Mathf.FloorToInt(height) : height;
    }

    private void CreateMesh()
    {
        mesh = new Mesh();
        mesh.name = "Generated";
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        mesh.vertices = meshVertices.ToArray();
        mesh.normals = meshNormals.ToArray();
        mesh.triangles = meshIndices.ToArray();

        if (GetComponent<MeshFilter>() != null)
        {
            GetComponent<MeshFilter>().sharedMesh = mesh;
        }
        if (GetComponent<MeshCollider>())
        {
            GetComponent<MeshCollider>().sharedMesh = mesh;
        }
    }
}
