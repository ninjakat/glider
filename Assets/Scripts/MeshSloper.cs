﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshSloper : MonoBehaviour
{
    public float Slope = 0.5f;
    public float HeightBoost = 1.0f;
    public Mesh InputMesh;
    public Vector2 Center;

    public void Meshalize()
    {
        if (InputMesh)
        {
            Mesh mesh = new Mesh();

            Vector3[] meshVertices = InputMesh.vertices;
            for (int i = 0; i < meshVertices.Length; ++i)
            {
                Vector3 v = meshVertices[i];
                float distance = (new Vector2(v.x, v.z) - Center).magnitude;
                float newY = v.y * HeightBoost - distance * Slope;
                meshVertices[i] = new Vector3(v.x, newY, v.z);
            }

            mesh.name = "Generated";
            mesh.vertices = meshVertices;
            mesh.triangles = InputMesh.triangles;
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            GetComponent<MeshFilter>().sharedMesh = mesh;
        }
    }
}
