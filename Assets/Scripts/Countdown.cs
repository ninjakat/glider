﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Countdown : MonoBehaviour
{
    public int NumSeconds = 3;
    public Rigidbody Body;

    TextMesh text;
    float startTime;

    void Start()
    {
        text = GetComponent<TextMesh>();
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        int timeLeft = NumSeconds - (int)(Time.time - startTime);
        if (timeLeft >= 0)
        {
            text.text = timeLeft.ToString();
        }

        if (timeLeft == 0)
        {
            Body.useGravity = true;
        }

        if (Input.GetKeyDown("r"))
        {
            startTime = Time.time;
            Body.useGravity = false;
            Body.velocity = Vector3.zero;
            Body.angularVelocity = Vector3.zero;
        }
    }
}
