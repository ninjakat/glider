﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ProceduralTerrain))]
public class MapTileEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ProceduralTerrain script = (ProceduralTerrain)target;
        if (GUILayout.Button("Init"))
        {
            script.Initialize();
        }
        if (GUILayout.Button("MoveRight"))
        {
            script.MoveRight();
        }
        if (GUILayout.Button("MoveLeft"))
        {
            script.MoveLeft();
        }
        if (GUILayout.Button("MoveForward"))
        {
            script.MoveForward();
        }
        if (GUILayout.Button("MoveBack"))
        {
            script.MoveBack();
        }
    }
}