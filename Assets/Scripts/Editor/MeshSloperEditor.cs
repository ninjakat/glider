﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MeshSloper))]
public class MeshSloperEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        MeshSloper script = (MeshSloper)target;
        if (GUILayout.Button("Meshalize"))
        {
            script.Meshalize();
        }
    }
}
