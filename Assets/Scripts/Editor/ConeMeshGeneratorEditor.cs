﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ConeMeshGenerator))]
public class ConeMeshGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        ConeMeshGenerator script = (ConeMeshGenerator)target;
        if (GUILayout.Button("Meshalize"))
        {
            script.Meshalize();
        }
    }
}
