﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTile
{
    public static Mesh MakePlaneMesh(int size)
    {   
        Mesh mesh = new Mesh();
        mesh.vertices = MakeVertices(size);
        mesh.triangles = MakeIndices(size);
        mesh.RecalculateNormals();

        return mesh;
    }

    static Vector3[] MakeVertices(int size)
    {
        int numVertices = (size + 1) * (size + 1);
        Vector3[] vertices = new Vector3[numVertices];

        int i = 0;
        for (int y = 0; y < size + 1; y++)
        {
            for (int x = 0; x < size + 1; x++)
            {
                vertices[i++] = new Vector3(x, 0, y);
            }
        }

        return vertices;
    }

    static int[] MakeIndices(int size)
    {
        int numIndices = size * size * 6;
        int[] indices = new int[numIndices];

        int baseIndex = 0;
        int i = 0;
        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                indices[i++] = (baseIndex + 1);
                indices[i++] = (baseIndex + size + 1);
                indices[i++] = (baseIndex + size + 2);

                indices[i++] = (baseIndex + size + 1);
                indices[i++] = (baseIndex + 1);
                indices[i++] = (baseIndex);

                baseIndex++;
            }
            baseIndex++;
        }

        return indices;
    }
}
