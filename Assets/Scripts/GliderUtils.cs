﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GliderUtils
{
    // Borrowed from UE4
    public static Vector3 RInterp(Vector3 from, Vector3 to, float speed)
    {
        Vector3 delta = NormalizeRotation(to - from);
        Vector3 deltaMove = delta * Mathf.Clamp01(speed * Time.deltaTime);
        return from + deltaMove;
    }

    public static Vector3 NormalizeRotation(Vector3 rotation)
    {
        return new Vector3(NormalizeAngle(rotation.x), NormalizeAngle(rotation.y), NormalizeAngle(rotation.z));
    }

    public static float NormalizeAngle(float degrees)
    {
        degrees %= 360.0f;
        return degrees > 180.0f ? degrees - 360.0f :
               degrees < -180.0f ? degrees + 360.0f :
               degrees;
    }

    public static string GetTimeString(float start, float stop)
    {
        return ((int)((stop - start) * 1000f)) + "ms";
    }

    // Slerps from a to b according to t, on the horizontal (XZ) plane
    public static Vector3 HSlerp(Vector3 a, Vector3 b, float t)
    {
        Vector3 ha = new Vector3(a.x, 0, a.z);
        Vector3 hb = new Vector3(b.x, 0, b.z);

        return Vector3.Slerp(ha, hb, t) + Vector3.up * Mathf.Lerp(a.y, b.y, t);
    }
}
