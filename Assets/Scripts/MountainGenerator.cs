﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainGenerator : MonoBehaviour
{
    public AnimationCurve Slope;
    public GameObject Template;
    public float Step = 0.01f;
    public float MinSampleDistance = 0.1f;

    // Start is called before the first frame update
    void Start()
    {

        //float dx = 1.0f / (float)NumSamples;
        //
        //for (int i = 0; i < NumSamples; ++i)
        //{
        //    float x = (float)i / (float)NumSamples;
        //    float y = Slope.Evaluate(x);
        //
        //    
        //    float dy = Slope.Evaluate(x + dx) - y;
        //
        //    GameObject go = GameObject.Instantiate(Template);
        //    go.transform.parent = transform;
        //    go.transform.localPosition = new Vector3(x, y, 0);
        //    go.transform.localRotation = Quaternion.LookRotation(new Vector3(dx, dy, 0));
        //}
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.matrix = transform.localToWorldMatrix;

        float x = 0.0f;
        Vector2 lastSample = new Vector2(x, Slope.Evaluate(x));

        while (x < 1.0f)
        {
            x += Step;
            Vector2 newSample = new Vector2(x, Slope.Evaluate(x));
            if (Vector2.Distance(lastSample, newSample) >= MinSampleDistance)
            {
                Gizmos.DrawSphere(newSample, MinSampleDistance * 0.5f);
                lastSample = newSample;
            }
        }
    }
}
