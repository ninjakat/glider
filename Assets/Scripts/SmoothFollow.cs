﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
    public Transform Follow = null;
    public Vector3 Offset = -Vector3.right;
    [Range(0.0f, 1.0f)]
    public float Smoothness = 0.5f;

    private void FixedUpdate()
    {
        if (Follow != null)
        {
            Vector3 targetPos = Follow.position + Offset;
            transform.position += (targetPos - transform.position) * (1.0f - Smoothness);
            transform.LookAt(Follow);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (Follow != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(Follow.position, Follow.position + Offset);

            if (!UnityEditor.EditorApplication.isPlaying)
            {
                transform.position = Follow.position + Offset;
            }
        }
    }
}
