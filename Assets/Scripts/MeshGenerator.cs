﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    public int Width = 32;
    public int Height = 32;
    public float Radius = 3.0f;

    // List of our vertices
    List<Vector3> points = null;
    List<Vector3> normals = null;

    // Maps an index in 'points' to a list of its connected indices
    Dictionary<int, List<int>> edges = null;

    // List of 3 indices in 'points' that form each triangle
    List<int> triangles = null;

    // The final mesh for rendering
    Mesh mesh = null;

    void Start()
    {

    }

    public void Meshalize()
    {
        float t0 = Time.realtimeSinceStartup;

        List<Tuple<int, int>> samples = NoiseGenerator.GenerateBlueNoise(Width, Height, Radius);

        float t1 = Time.realtimeSinceStartup;

        points = new List<Vector3>();
        CreatePoints(samples);

        edges = new Dictionary<int, List<int>>();
        for (int i = 0; i < points.Count; ++i)
        {
            CreateEdges(i);
        }

        triangles = new List<int>();
        for (int i = 0; i < points.Count; ++i)
        {
            CreateTriangles(i);
        }

        normals = new List<Vector3>();
        CreateNormals();

        float t2 = Time.realtimeSinceStartup;

        mesh = new Mesh();
        CreateMesh();

        float t3 = Time.realtimeSinceStartup;

        int execTimeMs = (int)((t1 - t0) * 1000f);
        Debug.Log("Noise gen: " + execTimeMs + "ms");
        execTimeMs = (int)((t2 - t1) * 1000f);
        Debug.Log("Points gen: " + execTimeMs + "ms");
        execTimeMs = (int)((t3 - t2) * 1000f);
        Debug.Log("Mesh gen: " + execTimeMs + "ms");
        Debug.Log("Poly count: " + points.Count);

        // TODO: repair holes / crossed over (delaunay)
        // TODO: speed up noise generation with sorted list or something
        // TODO: make generation "advance"
    }

    private void CreatePoints(List<Tuple<int, int>> samples)
    {
        foreach (Tuple<int, int> sample in samples)
        {
            float z = UnityEngine.Random.value * Radius;
            points.Add(new Vector3(sample.Item1, sample.Item2, z));
        }
    }

    private void CreateEdges(int index)
    {
        edges[index] = new List<int>();

        // Note: we downcast to Vector2
        Vector2 thisPoint = points[index];
        for (int i = 0; i < points.Count; ++i)
        {
            // TODO: real delaunay diagram
            if (i != index)
            {
                Vector2 otherPoint = points[i];
                float distance = (otherPoint - thisPoint).magnitude;

                if (distance < Radius * 2.0f)
                {
                    edges[index].Add(i);
                }
            }
        }
    }

    static float GetSignedAngle(Vector2 direction)
    {
        direction.Normalize();
        return Mathf.Acos(direction.x) * Mathf.Sign(direction.y);
    }

    private void CreateTriangles(int index)
    {
        // Sort edges depending on angle
        Vector3 center = points[index];
        List<int> otherIndices = edges[index];

        otherIndices.Sort(
            (a, b) => (GetSignedAngle(points[a] - center) < GetSignedAngle(points[b] - center) ? -1 : 1)
        );

        // For each pair of edges, generate triangle if they are linked
        int n = otherIndices.Count;
        for (int i = 0; i < n; ++i)
        {
            int a = otherIndices[i];
            int b = otherIndices[(i + 1) % n];

            if (edges[a].Contains(b))
            {
                triangles.Add(index);
                triangles.Add(a);
                triangles.Add(b);
            }
        }
    }

    private void CreateNormals()
    {
        //for (int i = 0; i < triangles.Count - 2; i += 3)
        //{
        //    Vector3 a = points[triangles[i + 0]];
        //    Vector3 b = points[triangles[i + 1]];
        //    Vector3 c = points[triangles[i + 2]];
        //
        //    Vector3 normal = Vector3.Cross(b - a, c - a).normalized;
        //    normals.Add(normal);
        //}
    }

    private void CreateMesh()
    {
        //mesh.vertices = points.ToArray();
        //mesh.normals = normals.ToArray();
        //mesh.triangles = triangles.ToArray();

        List<Vector3> meshVertices = new List<Vector3>();
        List<Vector3> meshNormals = new List<Vector3>();
        List<int> meshIndices = new List<int>();

        for (int i = 0; i < triangles.Count - 2; i += 3)
        {
            Vector3 a = points[triangles[i + 0]];
            Vector3 b = points[triangles[i + 1]];
            Vector3 c = points[triangles[i + 2]];

            Vector3 normal = Vector3.Cross(b - a, c - a).normalized;

            meshVertices.Add(a);
            meshVertices.Add(b);
            meshVertices.Add(c);

            meshNormals.Add(normal);
            meshNormals.Add(normal);
            meshNormals.Add(normal);

            meshIndices.Add(i + 0);
            meshIndices.Add(i + 1);
            meshIndices.Add(i + 2);
        }

        mesh.vertices = meshVertices.ToArray();
        mesh.normals = meshNormals.ToArray();
        mesh.triangles = meshIndices.ToArray();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.matrix = transform.localToWorldMatrix;

        if (points != null && edges != null)
        {
            if (mesh != null)
            {
                Gizmos.color = Color.white;
                Gizmos.DrawMesh(mesh);
            }

            Gizmos.color = new Color(0, 1, 0, 0.3f);
            foreach (int index in edges.Keys)
            {
                Vector3 from = points[index];

                foreach (int otherIndex in edges[index])
                {
                    Vector3 to = points[otherIndex];
                    Gizmos.DrawLine(from, to);
                }
            }

            Gizmos.color = new Color(1, 0, 0, 0.5f);
            foreach (Vector3 p in points)
            {
                Gizmos.DrawSphere(p, 0.1f * Radius);
            }
        }
    }
}
