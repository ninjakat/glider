﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralTerrain : MonoBehaviour
{
    public int TileSize = 10;
    public float NoiseScale = 0.1f;
    public float DensityScale = 0.1f;
    public float DensityScale2 = 0.1f;
    public float HeightScale = 1f;
    public Material Material;
    public Transform HotSpot;
    public float Slope = 1.0f;

    public enum Direction
    {
        UP, DOWN, LEFT, RIGHT
    }

    static float BigOffset = 5000f;

    [System.Serializable]
    struct TerrainTile
    {
        public Transform transform;
        public Mesh mesh;
        public MeshCollider collider;
    }

    // Terrain tiles, stored as follow:
    // z
    // ↑
    // 6 7 8
    // 3 4 5
    // 0 1 2 → x
    [SerializeField]
    [HideInInspector]
    TerrainTile[] tiles = new TerrainTile[9];

    private void Start()
    {
        Initialize();
    }

    private void Update()
    {
        if (HotSpot != null)
        {
            float t1 = Time.realtimeSinceStartup;

            // Assume we have no rotation and no scale
            Vector3 localHotSpot = HotSpot.position - tiles[0].transform.position;

            bool moved = true;
            if (localHotSpot.x > TileSize * 2f)
            {
                MoveRight();
            }
            else if (localHotSpot.x < TileSize)
            {
                MoveLeft();
            }
            else if (localHotSpot.z > TileSize * 2f)
            {
                MoveForward();
            }
            else if (localHotSpot.z < TileSize)
            {
                MoveBack();
            }
            else
            {
                moved = false;
            }

            float t2 = Time.realtimeSinceStartup;
            if (moved)
            {
                //Debug.Log("moved " + GliderUtils.GetTimeString(t1, t2));
            }
        }
    }

    public void Initialize()
    {
        foreach(TerrainTile tile in tiles)
        {
            if (tile.transform != null)
            {
                DestroyImmediate(tile.transform.gameObject);
            }
        }

        int tileId = 0;
        for (int j = 0; j < 3; ++j)
        {
            for (int i = 0; i < 3; ++i)
            {
                GameObject tile = new GameObject();
                tile.transform.parent = transform;
                tile.name = "MapTile" + j + "_" + i;

                Mesh mesh = MapTile.MakePlaneMesh(TileSize);
                mesh.name = tile.name + "_mesh";

                tile.AddComponent<MeshRenderer>().material = Material;
                tile.AddComponent<MeshFilter>().mesh = mesh;
                MeshCollider collider = tile.AddComponent<MeshCollider>();
                tile.transform.localPosition = new Vector3(i * TileSize, 0, j * TileSize);

                tiles[tileId].transform = tile.transform;
                tiles[tileId].mesh = mesh;
                tiles[tileId].collider = collider;

                RecalculateHeights(ref tiles[tileId]);

                ++tileId;
            }
        }

        StitchNormals();
    }

    static private float GetPerlinHeight(float u, float v)
    {
        float noise = Mathf.PerlinNoise(u, v) - 0.5f;
        float sign = Mathf.Sign(noise);
        float abs = Mathf.Abs(noise);

        noise = sign * Mathf.Pow(abs, 0.5f) + 0.5f;
        return Mathf.Clamp01(noise);
    }

    private void RecalculateHeights(ref TerrainTile tile)
    {
        Vector3[] vertices = tile.mesh.vertices;
        Vector3 offset = tile.transform.localPosition;

        for (int i = 0; i < vertices.Length; ++i)
        {
            Vector2 planarPosition = new Vector2(vertices[i].x + offset.x, vertices[i].z + offset.z);
            float slope = -(planarPosition - TileSize * 1.5f * Vector2.one).magnitude * Slope;

            // Perlin noise has a symmetry pattern at 0, so we add a big offset here
            // TODO: Variable density, by integrating a noise function to compute the offset
            float densityIntegral = (Mathf.Cos(planarPosition.magnitude * DensityScale) * DensityScale2 + 1f);
            Vector2 uv = BigOffset * Vector2.one / NoiseScale + planarPosition * densityIntegral / NoiseScale;
            float noise = GetPerlinHeight(uv.x, uv.y);

            vertices[i].y = slope + noise * HeightScale;
        }

        tile.mesh.vertices = vertices;

        tile.mesh.RecalculateNormals();
        tile.mesh.RecalculateBounds();
        tile.collider.sharedMesh = tile.mesh;
    }

    //void MoveRight(int t)
    //{
    //    TerrainTile baseTile = tiles[t];
    //    tiles[t] = tiles[t + 1];
    //    tiles[t + 1] = tiles[t + 2];
    //    tiles[t + 2] = baseTile;
    //
    //    baseTile.transform.localPosition += Vector3.right * 3 * TileSize;
    //    RecalculateHeights(ref baseTile);
    //
    //    Mesh leftMesh = tiles[t + 1].mesh;
    //    Mesh rightMesh = tiles[t + 2].mesh;
    //    StitchNormalsHorizontal(leftMesh, rightMesh);
    //}
    //
    //void MoveLeft(int t)
    //{
    //    TerrainTile baseTile = tiles[t];
    //    tiles[t] = tiles[t - 1];
    //    tiles[t - 1] = tiles[t - 2];
    //    tiles[t - 2] = baseTile;
    //
    //    baseTile.transform.localPosition -= Vector3.right * 3 * TileSize;
    //    RecalculateHeights(ref baseTile);
    //
    //    Mesh leftMesh = tiles[t - 2].mesh;
    //    Mesh rightMesh = tiles[t - 1].mesh;
    //    StitchNormalsHorizontal(leftMesh, rightMesh);
    //}
    //
    //void MoveForward(int t)
    //{
    //    TerrainTile baseTile = tiles[t];
    //    tiles[t] = tiles[t + 3];
    //    tiles[t + 3] = tiles[t + 6];
    //    tiles[t + 6] = baseTile;
    //
    //    baseTile.transform.localPosition += Vector3.forward * 3 * TileSize;
    //    RecalculateHeights(ref baseTile);
    //
    //    Mesh upMesh = tiles[t + 6].mesh;
    //    Mesh downMesh = tiles[t + 3].mesh;
    //    StitchNormalsVertical(upMesh, downMesh);
    //}
    //
    //void MoveBack(int t)
    //{
    //    TerrainTile baseTile = tiles[t];
    //    tiles[t] = tiles[t - 3];
    //    tiles[t - 3] = tiles[t - 6];
    //    tiles[t - 6] = baseTile;
    //
    //    baseTile.transform.localPosition -= Vector3.forward * 3 * TileSize;
    //    RecalculateHeights(ref baseTile);
    //
    //    Mesh upMesh = tiles[t - 3].mesh;
    //    Mesh downMesh = tiles[t - 6].mesh;
    //    StitchNormalsVertical(upMesh, downMesh);
    //}

    private void MoveTile(int t, int inc, Vector3 dir)
    {
        TerrainTile baseTile = tiles[t];
        tiles[t] = tiles[t + inc];
        tiles[t + inc] = tiles[t + inc * 2];
        tiles[t + inc * 2] = baseTile;

        baseTile.transform.localPosition += dir * 3 * TileSize;
        RecalculateHeights(ref baseTile);
    }

    private void StitchNormals()
    {
        StitchNormalsHorizontal(tiles[0].mesh, tiles[1].mesh);
        StitchNormalsHorizontal(tiles[1].mesh, tiles[2].mesh);
        StitchNormalsHorizontal(tiles[3].mesh, tiles[4].mesh);
        StitchNormalsHorizontal(tiles[4].mesh, tiles[5].mesh);
        StitchNormalsHorizontal(tiles[6].mesh, tiles[7].mesh);
        StitchNormalsHorizontal(tiles[7].mesh, tiles[8].mesh);

        StitchNormalsVertical(tiles[6].mesh, tiles[3].mesh);
        StitchNormalsVertical(tiles[3].mesh, tiles[0].mesh);
        StitchNormalsVertical(tiles[7].mesh, tiles[4].mesh);
        StitchNormalsVertical(tiles[4].mesh, tiles[1].mesh);
        StitchNormalsVertical(tiles[8].mesh, tiles[5].mesh);
        StitchNormalsVertical(tiles[5].mesh, tiles[2].mesh);
    }

    public void MoveRight()
    {
        MoveTile(0, 1, Vector3.right);
        MoveTile(3, 1, Vector3.right);
        MoveTile(6, 1, Vector3.right);

        StitchNormals(); return;

        //Mesh upMesh = tiles[8].mesh;
        //Mesh midMesh = tiles[5].mesh;
        //Mesh downMesh = tiles[2].mesh;
        //StitchNormalsVertical(upMesh, midMesh);
        //StitchNormalsVertical(midMesh, downMesh);
    }

    public void MoveLeft()
    {
        MoveTile(2, -1, Vector3.left);
        MoveTile(5, -1, Vector3.left);
        MoveTile(8, -1, Vector3.left);
        StitchNormals(); return;

        Mesh upMesh = tiles[6].mesh;
        Mesh midMesh = tiles[3].mesh;
        Mesh downMesh = tiles[0].mesh;
        StitchNormalsVertical(upMesh, midMesh);
        StitchNormalsVertical(midMesh, downMesh);
    }

    public void MoveForward()
    {
        MoveTile(0, 3, Vector3.forward);
        MoveTile(1, 3, Vector3.forward);
        MoveTile(2, 3, Vector3.forward);
        StitchNormals(); return;

        Mesh leftMesh = tiles[6].mesh;
        Mesh midMesh = tiles[7].mesh;
        Mesh rightMesh = tiles[8].mesh;
        StitchNormalsHorizontal(leftMesh, midMesh);
        StitchNormalsHorizontal(midMesh, rightMesh);
    }

    public void MoveBack()
    {
        MoveTile(6, -3, Vector3.back);
        MoveTile(7, -3, Vector3.back);
        MoveTile(8, -3, Vector3.back);
        StitchNormals(); return;

        Mesh leftMesh = tiles[0].mesh;
        Mesh midMesh = tiles[1].mesh;
        Mesh rightMesh = tiles[2].mesh;
        StitchNormalsHorizontal(leftMesh, midMesh);
        StitchNormalsHorizontal(midMesh, rightMesh);
    }

    void StitchNormalsHorizontal(Mesh leftMesh, Mesh rightMesh)
    {
        Vector3[] leftNormals = leftMesh.normals;
        Vector3[] rightNormals = rightMesh.normals;

        for (int i = 0; i < TileSize + 1; ++i)
        {
            int leftIndex = (TileSize) + i * (TileSize + 1);
            int rightIndex = i * (TileSize + 1);

            Vector3 averageNormal = (leftNormals[leftIndex] + rightNormals[rightIndex]).normalized;
            leftNormals[leftIndex] = rightNormals[rightIndex] = averageNormal;
        }

        leftMesh.normals = leftNormals;
        rightMesh.normals = rightNormals;
    }

    void StitchNormalsVertical(Mesh upMesh, Mesh downMesh)
    {
        Vector3[] upNormals = upMesh.normals;
        Vector3[] downNormals = downMesh.normals;

        for (int i = 0; i < TileSize + 1; ++i)
        {
            int upIndex = i;
            int downIndex = (TileSize + 1) * (TileSize) + i;

            Vector3 averageNormal = (upNormals[upIndex] + downNormals[downIndex]).normalized;
            upNormals[upIndex] = downNormals[downIndex] = averageNormal;
        }

        upMesh.normals = upNormals;
        downMesh.normals = downNormals;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        foreach (TerrainTile tile in tiles)
        {
            if (tile.transform == null)
            {
                continue;
            }

            Mesh mesh = tile.mesh;
            Vector3[] vertices = mesh.vertices;
            Vector3[] normals = mesh.normals;
        
            for (int i = 0; i < vertices.Length; ++i)
            {
                Vector3 vPos = tile.transform.position + vertices[i];
                Gizmos.DrawLine(vPos, vPos + normals[i]);
            }
        }
    }
}