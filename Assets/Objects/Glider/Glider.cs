﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Networking;

public class Glider : NetworkBehaviour
{
    public float TurnSpeed = 60.0f;
    public float MinAngle = -20.0f;
    public float MaxAngle = 85.0f;
    public float RotationInterpSpeed = 5.0f;

    public float ThrustFactor = 1.0f;
    public float ThrustInterpSpeed = 1.0f;
    public AnimationCurve DiveThrustCurve;

    public float LiftFactor = 10.0f;
    public AnimationCurve SpeedLiftCurve;
    public AnimationCurve PitchLiftCurve;

    public float GravityFactor = 1.0f;

    Rigidbody body;
    Animator anim;
    InputActionAsset actions;
    Vector3 resetPosition;
    Quaternion resetRotation;

    float turnInput = 0.0f;
    float pitchInput = 0.0f;

    float pitchTarget = 0.0f;
    float yawTarget = 0.0f;

    float thrust = 0.0f;
    float currentPitch = 0.0f;


    void Start()
    {
        body = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        resetPosition = transform.position;
        resetRotation = transform.rotation;
    }

    public override void OnStartAuthority()
    {
        base.OnStartAuthority();
    
        RegisterInputs();
    }

    private void RegisterInputs()
    {
        // TODO: Skip for dedicated servers
        PlayerInput input = GetComponent<PlayerInput>();
        input.actions["move"].performed += OnMoveInput;
        input.actions["move"].canceled += OnMoveInput;
        input.actions["dive"].performed += OnDiveInput;
        input.actions["dive"].canceled += OnDiveInput;
        input.actions["reset"].performed += OnResetPosition;
    }

    // Work around Unity removing the InputUser but not adding it back correctly.
    private void OnEnable()
    {
        if (actions != null)
            GetComponent<PlayerInput>().actions = actions;
    }

    private void OnDisable()
    {
        PlayerInput input = GetComponent<PlayerInput>();
        actions = input.actions;
        input.actions = null;
    }

    private void OnMoveInput(InputAction.CallbackContext context)
    {
        Vector2 input = context.ReadValue<Vector2>();
        turnInput = input.x;
        pitchInput = input.y;
    }

    private void OnDiveInput(InputAction.CallbackContext context)
    {
        // TODO: Implement some fast dive / slow down mechanics
    }

    private void OnResetPosition(InputAction.CallbackContext context)
    {
        transform.position = resetPosition;
        transform.rotation = resetRotation;
        body.velocity = Vector3.zero;
        body.angularVelocity = Vector3.zero;
    }

    void Update()
    {
        // TODO: slomo when pressing whatever
        // TODO: have some more dynamic to this value. Needs to counter balance when the player stops turning.
        anim.SetFloat("LeftRight", turnInput);
    }

    private void FixedUpdate()
    {
        //if (!hasAuthority)
        //{
        //    return;
        //}

        HandleInputs();
        ApplyForces();

        //Debug.DrawLine(transform.position, transform.position + transform.forward, Color.blue, 60f);
    }

    private void HandleInputs()
    {
        float roll = -turnInput * 30.0f;

        pitchTarget += pitchInput * TurnSpeed * Time.deltaTime;
        pitchTarget = Mathf.Clamp(pitchTarget, MinAngle, MaxAngle);

        yawTarget += turnInput * TurnSpeed * Time.deltaTime;
        yawTarget = GliderUtils.NormalizeAngle(yawTarget);

        Vector3 eulerAngles = transform.rotation.eulerAngles;
        eulerAngles = GliderUtils.RInterp(eulerAngles, new Vector3(pitchTarget, yawTarget, roll), RotationInterpSpeed);
        transform.rotation = Quaternion.Euler(eulerAngles);

        currentPitch = pitchTarget;
    }

    private void ApplyForces()
    {
        float speed = body.velocity.magnitude;

        float lift = LiftFactor * SpeedLiftCurve.Evaluate(speed) * PitchLiftCurve.Evaluate(currentPitch);

        float thrustTarget = ThrustFactor * DiveThrustCurve.Evaluate(currentPitch);
        thrust = Mathf.Lerp(thrust, thrustTarget, ThrustInterpSpeed * Time.deltaTime);

        // TODO: add lateral force when turning much
        Vector3 force = lift * transform.up + thrust * transform.forward + GravityFactor * Vector3.down;

        body.AddForce(force, ForceMode.Acceleration);
    }
}
