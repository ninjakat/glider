﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkIdentity))]
public class GliderNetworkController : NetworkBehaviour
{
    GameObject _pawn;

    private void Start()
    {
        if (isServer)
        {
            _pawn = GameObject.Instantiate(GliderNetworkManager.playerTemplate);
            _pawn.transform.position = transform.position;
            _pawn.transform.rotation = transform.rotation;

            // Give authority to the local player
            NetworkServer.SpawnWithClientAuthority(_pawn, connectionToClient);
        }
    }
}
