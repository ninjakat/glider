﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GliderNetworkManager : NetworkManager
{
    public GameObject _playerTemplate;

    static public GameObject playerTemplate { get { return (singleton as GliderNetworkManager)?._playerTemplate; } }

    private void Awake()
    {
        if (_playerTemplate != null && !spawnPrefabs.Contains(_playerTemplate))
        {
            spawnPrefabs.Add(_playerTemplate);
        }
    }
}
