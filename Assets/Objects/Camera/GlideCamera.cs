﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlideCamera : MonoBehaviour
{
    public Transform Follow;
    public float TargetDistance = 1.0f;
    public float Speed = 1.0f;
    public float AlignmentSpeed = 1.0f;
    public bool BoostAlignment = false;
    public float RotationSpeed = 1.0f;
    public bool BoostRotation = false;

    private Vector3 previousPosition;
    private Vector3 previousForward;


    private void Start()
    {
        previousPosition = Follow.position;
        previousForward = Follow.forward;
    }

    private void FixedUpdate()
    {
        AlignBehindTarget();
        DistanceCheck();
        LookAtTarget();

        Debug.DrawLine(transform.position, transform.position + transform.forward, Color.red, 60f);

        previousPosition = Follow.position;
        previousForward = Follow.forward;
    }

    private void AlignBehindTarget()
    {
        Vector3 deltaPos = Follow.position - transform.position;
        Vector3 direction = deltaPos.normalized;

        // TODO: overshoot the alignment when the Follow is rotating to show where we're going instead of dragging behind
        // TODO: camera shouldn't go like shit when going up
        Vector3 deltaRot = GliderUtils.NormalizeRotation(Follow.eulerAngles - Quaternion.LookRotation(direction).eulerAngles);
        Vector3 alignmentTarget = Quaternion.Euler(0, deltaRot.y, 0) * deltaPos;
        float boost = BoostAlignment ? Mathf.Abs(deltaRot.y) : 1f;
        Vector3 alignmentDelta = deltaPos - GliderUtils.HSlerp(deltaPos, alignmentTarget, boost * AlignmentSpeed * Time.deltaTime);
        transform.position += alignmentDelta;
    }

    private void DistanceCheck()
    {
        Vector3 deltaPos = Follow.position - transform.position;
        Vector3 direction = deltaPos.normalized;
        float distance = deltaPos.magnitude;

        float distanceDelta = distance - Mathf.Lerp(distance, TargetDistance, Mathf.Abs(distance - TargetDistance) * Speed * Time.deltaTime);
        transform.position += distanceDelta * direction;
    }

    private void LookAtTarget()
    {
        Vector3 deltaPos = Follow.position - transform.position;
        Vector3 direction = deltaPos.normalized;

        Quaternion currentRotation = transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        float boost = BoostRotation ? Mathf.Acos(Vector3.Dot(transform.forward, direction)) / Mathf.PI : 1f;
        transform.rotation = Quaternion.Lerp(currentRotation, targetRotation, boost * RotationSpeed * Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        //Gizmos.DrawRay(transform.position, transform.forward);
    }
}
